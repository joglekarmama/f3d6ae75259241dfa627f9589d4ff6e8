import React, { useContext, useState } from 'react';
import Styled from 'styled-components';
import config from 'visual-config-exposer';

import { GameContext } from './context/gameContext';
import PreQuiz from './components/preQuiz/preQuiz';
import PostQuiz from './components/postQuiz/postQuiz';
import Quiz from './components/quiz/quiz';
import './app.css';

const Main = Styled.main`
background-image: url(${config.preQuizScreen.bgImg});
background-size: cover;
background-repeat: no-repeat;
background-position: center;
`;

const App = () => {
  const gameContext = useContext(GameContext);

  const [soundOn, setSoundOn] = useState(false);

  const soundHandler = () => {
    setSoundOn(!soundOn);
  };

  return (
    <Main className="main">
      {gameContext.screen === 'PRE' && (
        <PreQuiz soundOn={soundOn} soundHandler={soundHandler} />
      )}
      {gameContext.screen === 'MAIN' && (
        <Quiz soundOn={soundOn} soundHandler={soundHandler} />
      )}
      {gameContext.screen === 'POST' && <PostQuiz />}
    </Main>
  );
};

export default App;
